package com.vazhasapp.assignment18.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.vazhasapp.assignment18.databinding.FragmentMainBinding
import com.vazhasapp.assignment18.viewmodels.MoviesViewModel

class MainFragment :
    BaseFragment<FragmentMainBinding, MoviesViewModel>(
        FragmentMainBinding::inflate,
        MoviesViewModel::class.java
    ) {

    override fun initialized(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        mViewModel.getHelloWorld()
    }
}