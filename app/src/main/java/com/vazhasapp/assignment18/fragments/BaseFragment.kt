package com.vazhasapp.assignment18.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.vazhasapp.assignment18.R
import com.vazhasapp.assignment18.viewmodels.MoviesViewModel
import kotlin.reflect.KClass

typealias FragmentInflater<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding, VM : ViewModel>(
    private val fragmentInflater: FragmentInflater<VB>,
    viewModel: Class<VM>
) : Fragment() {

    private var _binding: VB? = null
    private val binding get() = _binding!!
    
    protected val mViewModel: VM by lazy {
        ViewModelProvider(this).get(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = fragmentInflater.invoke(inflater, container, false)

        initialized(layoutInflater, container)

        return binding.root
    }

    protected abstract fun initialized(inflater: LayoutInflater, container: ViewGroup?)

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}